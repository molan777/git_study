# git_study

#### 介绍
用于学习git的创建和使用。

#### 软件架构
软件架构说明


# 1.git的版本
## 什么是版本控制
版本控制（Revision control）是一种在开发的过程中用于管理我们对文件、目录或工程等内容的修改历史，方便查看更改历史记录，备份以便恢复以前的版本的软件工程技术。

- 实现跨区域多人协同开发
- 追踪和记载一个或者多个文件的历史记录
- 组织和保护你的源代码和文档
- 统计工作量
- 并行开发、提高开发效率
- 跟踪记录整个软件的开发过程
- 减轻开发人员的负担，节省时间，同时降低人为错误

简单说就是用于管理多人协同开发项目的技术。
![](https://cdn.nlark.com/yuque/0/2022/jpeg/27233047/1653701477158-6d365ab7-cb2e-466a-998f-48fd4ca89bd7.jpeg#clientId=uec4524eb-b3b5-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=461&id=u9cc476ae&margin=%5Bobject%20Object%5D&originHeight=743&originWidth=450&originalType=url&ratio=1&rotation=0&showTitle=false&status=done&style=none&taskId=u251360c6-ebf2-41da-ae7c-001786d69b1&title=&width=279)
## 常见的版本控制工具
我们学习的东西，一定是当下最流行的！
主流的版本控制器有如下这些：

- **Git**
- **SVN**（Subversion）
- **CVS**（Concurrent Versions System）
- **VSS**（Micorosoft Visual SourceSafe）
- **TFS**（Team Foundation Server）
- Visual Studio Online
## 版本控制分类
**1、本地版本控制**
记录文件每次的更新，可以对每个版本做一个快照，或是记录补丁文件，适合个人用，如RCS。
![](https://cdn.nlark.com/yuque/0/2022/png/27233047/1653701530678-b02967eb-ba7c-4a15-a794-c11fa8b0e40f.png#clientId=uec4524eb-b3b5-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=364&id=u741ad6ac&margin=%5Bobject%20Object%5D&originHeight=523&originWidth=617&originalType=url&ratio=1&rotation=0&showTitle=false&status=done&style=none&taskId=ufa9ab55c-45d3-4ee1-aca6-81b8333de39&title=&width=430)
**2、集中版本控制  SVN**
所有的版本数据都保存在服务器上，协同开发者从服务器上同步更新或上传自己的修改
![](https://cdn.nlark.com/yuque/0/2022/png/27233047/1653701530583-28625b3d-cda5-4a15-b22d-c2edd12568b7.png#clientId=uec4524eb-b3b5-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=413&id=ub0533a07&margin=%5Bobject%20Object%5D&originHeight=608&originWidth=763&originalType=url&ratio=1&rotation=0&showTitle=false&status=done&style=none&taskId=u9eb93787-6442-4e28-8a7c-f9b78c7e19b&title=&width=518)
所有的版本数据都存在服务器上，用户的本地只有自己以前所同步的版本，如果不连网的话，用户就看不到历史版本，也无法切换版本验证问题，或在不同分支工作。而且，所有数据都保存在单一的服务器上，有很大的风险这个服务器会损坏，这样就会丢失所有的数据，当然可以定期备份。代表产品：SVN、CVS、VSS
**3、分布式版本控制 	Git**
每个人都拥有全部的代码！安全隐患！
所有版本信息仓库全部同步到本地的每个用户，这样就可以在本地查看所有版本历史，可以离线在本地提交，只需在连网时push到相应的服务器或其他用户那里。由于每个用户那里保存的都是所有的版本数据，只要有一个用户的设备没有问题就可以恢复所有的数据，但这增加了本地存储空间的占用。
不会因为服务器损坏或者网络问题，造成不能工作的情况！
![](https://cdn.nlark.com/yuque/0/2022/jpeg/27233047/1653701530704-f608fb80-12b9-47c5-aaf9-7acea8bbc21b.jpeg#clientId=uec4524eb-b3b5-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=567&id=u0a56644a&margin=%5Bobject%20Object%5D&originHeight=857&originWidth=765&originalType=url&ratio=1&rotation=0&showTitle=false&status=done&style=none&taskId=u4a40e6f1-0eb3-458a-b6a0-bf0eec75f05&title=&width=506)
## Git与SVN的主要区别
SVN是集中式版本控制系统，版本库是集中放在中央服务器的，而工作的时候，用的都是自己的电脑，所以首先要从中央服务器得到最新的版本，然后工作，完成工作后，需要把自己做完的活推送到中央服务器。集中式版本控制系统是必须联网才能工作，对网络带宽要求较高。
![image.png](https://cdn.nlark.com/yuque/0/2022/gif/27233047/1653701530565-f769193a-d58e-4fe0-b29d-0d797808ef69.gif#clientId=uec4524eb-b3b5-4&crop=0&crop=0&crop=1&crop=1&from=paste&id=u551495c5&margin=%5Bobject%20Object%5D&name=image.png&originHeight=1&originWidth=1&originalType=url&ratio=1&rotation=0&showTitle=false&size=70&status=done&style=none&taskId=u6e394c48-ba0d-4c67-9351-28b65aae318&title=)
Git是分布式版本控制系统，没有中央服务器，每个人的电脑就是一个完整的版本库，工作的时候不需要联网了，因为版本都在自己电脑上。协同的方法是这样的：比如说自己在电脑上改了文件A，其他人也在电脑上改了文件A，这时，你们两之间只需把各自的修改推送给对方，就可以互相看到对方的修改了。Git可以直接看到更新了哪些代码和文件！
**Git是目前世界上最先进的分布式版本控制系统。**

# Git的历史
同生活中的许多伟大事物一样，Git 诞生于一个极富纷争大举创新的年代。
Linux 内核开源项目有着为数众广的参与者。绝大多数的 Linux 内核维护工作都花在了提交补丁和保存归档的繁琐事务上(1991－2002年间)。到 2002 年，整个项目组开始启用一个专有的分布式版本控制系统 BitKeeper 来管理和维护代码。
Linux社区中存在很多的大佬！破解研究 BitKeeper ！
到了 2005 年，开发 BitKeeper 的商业公司同 Linux 内核开源社区的合作关系结束，他们收回了 Linux 内核社区免费使用 BitKeeper 的权力。这就迫使 Linux 开源社区(特别是 Linux 的缔造者 Linus Torvalds)基于使用 BitKeeper 时的经验教训，开发出自己的版本系统。（2周左右！） 也就是后来的 Git！
**Git是目前世界上最先进的分布式版本控制系统。**
Git是免费、开源的，最初Git是为辅助 Linux 内核开发的，来替代 BitKeeper！
![](https://cdn.nlark.com/yuque/0/2022/jpeg/27233047/1653701722940-8d78489a-4e81-4419-9788-2ea5880b4b4e.jpeg#clientId=uec4524eb-b3b5-4&crop=0&crop=0&crop=1&crop=1&from=paste&id=ud99cc941&margin=%5Bobject%20Object%5D&originHeight=332&originWidth=577&originalType=url&ratio=1&rotation=0&showTitle=false&status=done&style=none&taskId=u8dae8cc5-49d7-466a-97e9-4921730f68d&title=)
Linux和Git之父李纳斯·托沃兹（Linus Benedic Torvalds）1969、芬兰
# Git安装
官网下载太慢，我们可以使用淘宝镜像下载：http://npm.taobao.org/mirrors/git-for-windows/
下载完，一直下一步，安装即可。
安装完
![image.png](https://cdn.nlark.com/yuque/0/2022/png/27233047/1653702161794-3e1ce2c9-9fa9-4dcf-bfa1-63ebe56f3fe9.png#clientId=uec4524eb-b3b5-4&crop=0&crop=0&crop=1&crop=0.445&from=paste&height=618&id=uf0a0ef79&margin=%5Bobject%20Object%5D&name=image.png&originHeight=618&originWidth=408&originalType=binary&ratio=1&rotation=0&showTitle=false&size=64977&status=done&style=none&taskId=u7e5637ae-dada-4020-ab25-220e7a4578c&title=&width=408)
桌面右键也可以看到。
**Git Bash:unix与Linux风格命令行，使用最多，推荐最多**
**Git CMD：windows风格命令行号**
**Git GUI：图形化界面的Git，不建议初学者使用，尽量熟悉命令行**

# linux基础语法的使用
**回到上级目录：$cd.   .**
**进gitcode目录：$cd gitcode**
**显示当前目录地址：  $pwd**
**快速清屏： $clear**
**获取文件夹内目录：$ls**
**创建文件：$touch index.js**
**创建文件夹：$mkdir 文件夹名字**
**删除文件：$rm index.js**
**删除一个文件夹：$rm -r 目录名**
**切勿在linux中使用：rm -rf删除电脑中全部的文件**
**移动文件：$mv index.js 文件夹目录**
**查看用过的命令：$history**
**查看帮助：$help**
**退出：$exit**
**注释：#**

# Git配置
所有的配置文件都保留在本地
依次输入：
本地的配置：

- **$git config -1**

![image.png](https://cdn.nlark.com/yuque/0/2022/png/27233047/1653704121455-ed4d933a-72af-4eca-8ce5-134640dddef5.png#clientId=uec4524eb-b3b5-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=448&id=u33ad4475&margin=%5Bobject%20Object%5D&name=image.png&originHeight=448&originWidth=702&originalType=binary&ratio=1&rotation=0&showTitle=false&size=40382&status=done&style=none&taskId=ubb52ae39-6a2a-4205-91da-930122d5233&title=&width=702)
**查看系统的配置：**

- **$git config --system --list**

![image.png](https://cdn.nlark.com/yuque/0/2022/png/27233047/1653704213789-a3f26855-170f-4cfa-bc55-97ca9dd8872d.png#clientId=uec4524eb-b3b5-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=350&id=uea864799&margin=%5Bobject%20Object%5D&name=image.png&originHeight=350&originWidth=583&originalType=binary&ratio=1&rotation=0&showTitle=false&size=30823&status=done&style=none&taskId=u20282066-a5e0-4de8-ba40-fdf1ec1904b&title=&width=583)
**查看用户：**

- **$git config --global --list**

![image.png](https://cdn.nlark.com/yuque/0/2022/png/27233047/1653705952041-79de19b9-e9bd-4453-8bfc-a5069b5855e0.png#clientId=uec4524eb-b3b5-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=173&id=u667c2760&margin=%5Bobject%20Object%5D&name=image.png&originHeight=173&originWidth=561&originalType=binary&ratio=1&rotation=0&showTitle=false&size=17571&status=done&style=none&taskId=ua55d3e24-4b0a-4209-a1f0-a3c5da7030c&title=&width=561)
1）、Git\etc\gitconfig  ：Git 安装目录下的 gitconfig     --system 系统级
2）、C:\Users\Administrator\ .gitconfig    只适用于当前登录用户的配置  --global 全局
![image.png](https://cdn.nlark.com/yuque/0/2022/png/27233047/1653706473417-7f68f24c-33f4-47e8-998a-92c9cc669dda.png#clientId=uec4524eb-b3b5-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=552&id=u64845d7b&margin=%5Bobject%20Object%5D&name=image.png&originHeight=552&originWidth=820&originalType=binary&ratio=1&rotation=0&showTitle=false&size=58710&status=done&style=none&taskId=u4c1c7144-d7b7-4456-a805-06c489a27f6&title=&width=820)
![image.png](https://cdn.nlark.com/yuque/0/2022/png/27233047/1653706754472-53b7f085-b241-495b-a01c-c393ad26cb8e.png#clientId=uec4524eb-b3b5-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=312&id=u74b07fdd&margin=%5Bobject%20Object%5D&name=image.png&originHeight=312&originWidth=803&originalType=binary&ratio=1&rotation=0&showTitle=false&size=35415&status=done&style=none&taskId=u4807764d-cf69-4385-9c9f-2e745f029a5&title=&width=803)
**设置用户信息(这个是必须要配置的)：**
**$git config --global user.name "kuangshen"**
**$git config --global user.email "54546455@qq.com"**

# Git理论核心
Git本地有三个工作区域：工作目录（Working Directory）、暂存区(Stage/Index)、资源库(Repository或Git Directory)。如果在加上远程的git仓库(Remote Directory)就可以分为四个工作区域。文件在这四个区域之间的转换关系如下：
![](https://cdn.nlark.com/yuque/0/2022/png/27233047/1653707446956-6c8b3bd3-36a1-45a5-a10d-75439e2a62ad.png#clientId=uec4524eb-b3b5-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=397&id=u2b27479e&margin=%5Bobject%20Object%5D&originHeight=480&originWidth=500&originalType=url&ratio=1&rotation=0&showTitle=false&status=done&style=none&taskId=u5d05f13a-181c-42f9-9d0a-909111be4ab&title=&width=414)![image.png](https://cdn.nlark.com/yuque/0/2022/png/27233047/1653707403903-bf07e0b8-14a0-4ccd-a76b-eba2c3b109a0.png#clientId=uec4524eb-b3b5-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=399&id=u6bdc7745&margin=%5Bobject%20Object%5D&name=image.png&originHeight=399&originWidth=415&originalType=binary&ratio=1&rotation=0&showTitle=false&size=55458&status=done&style=none&taskId=ufc3e0b25-c1ad-415a-bfd6-f6e51106494&title=&width=415)

- Workspace：工作区，就是你平时存放项目代码的地方
- Index / Stage：暂存区，用于临时存放你的改动，事实上它只是一个文件，保存即将提交到文件列表信息
- Repository：仓库区（或本地仓库），就是安全存放数据的位置，这里面有你提交到所有版本的数据。其中HEAD指向最新放入仓库的版本
- ![image.png](https://cdn.nlark.com/yuque/0/2022/png/27233047/1653707592795-80730ae3-b05a-45c4-baea-d127b9d450ef.png#clientId=uec4524eb-b3b5-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=246&id=ubbbee5a3&margin=%5Bobject%20Object%5D&name=image.png&originHeight=246&originWidth=425&originalType=binary&ratio=1&rotation=0&showTitle=false&size=108596&status=done&style=none&taskId=u4ae4eeee-b8ae-44cc-9599-2eeb1f7beac&title=&width=425)
- Remote：远程仓库，托管代码的服务器，可以简单的认为是你项目组中的一台电脑用于远程数据交换

本地的三个区域确切的说应该是git仓库中HEAD指向的版本：
![image.png](https://cdn.nlark.com/yuque/0/2022/png/27233047/1653707910033-ef510950-c8b2-418b-8d0f-77146ffafb67.png#clientId=uec4524eb-b3b5-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=676&id=ua9e4a52b&margin=%5Bobject%20Object%5D&name=image.png&originHeight=676&originWidth=1014&originalType=binary&ratio=1&rotation=0&showTitle=false&size=195495&status=done&style=none&taskId=ua7ab25e0-f14f-459f-87d9-bd91be09282&title=&width=1014)
git的工作流程
![image.png](https://cdn.nlark.com/yuque/0/2022/png/27233047/1653707981331-985f3cb6-1141-4578-a4c3-0618fc379cb3.png#clientId=uec4524eb-b3b5-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=199&id=u902f2cef&margin=%5Bobject%20Object%5D&name=image.png&originHeight=199&originWidth=772&originalType=binary&ratio=1&rotation=0&showTitle=false&size=112165&status=done&style=none&taskId=ubf56c311-e30a-4797-b999-3d928802bf5&title=&width=772)
![image.png](https://cdn.nlark.com/yuque/0/2022/png/27233047/1653708001745-b6d4c52c-5b32-466e-9cac-41e79bfdf96c.png#clientId=uec4524eb-b3b5-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=375&id=ub3ab9e67&margin=%5Bobject%20Object%5D&name=image.png&originHeight=375&originWidth=1076&originalType=binary&ratio=1&rotation=0&showTitle=false&size=200457&status=done&style=none&taskId=ud650f885-4a99-45e7-8ed2-23cdcc1543b&title=&width=1076)
## 创建工作目录与常用指令
工作目录（WorkSpace)一般就是你希望Git帮助你管理的文件夹，可以是你项目的目录，也可以是一个空目录，建议不要有中文。
日常使用只要记住下图6个命令：
### ![](https://cdn.nlark.com/yuque/0/2022/png/27233047/1653708074692-127d45f4-d7ed-497f-8db7-45f26afa146e.png#clientId=uec4524eb-b3b5-4&crop=0&crop=0&crop=1&crop=1&from=paste&id=u6e098731&margin=%5Bobject%20Object%5D&originHeight=337&originWidth=1080&originalType=url&ratio=1&rotation=0&showTitle=false&status=done&style=none&taskId=u227b70b1-434d-400d-ab00-ec792138505&title=)
 git一些常用命令
创建远程仓库
**1.创建全新的仓库，需要Git管理的项目的根目录执行**
初始化git项目，在本地创建一个Git代码库：
**$git init**
**2.执行后可以看到仅仅在目录中多出了一个.git目录，关于版本等所有信息都在目录里。（文件夹是隐藏的，需要设置显示）**

## 克隆远程仓库
**1、另一种方式是克隆远程目录，由于是将远程服务器上的仓库完全镜像一份至本地！**
# 克隆一个项目和它的整个代码历史(版本信息)
克隆getee或者github的项目到本地来。
**$ git clone [url]  # https://gitee.com/kuangstudy/openclass.git**
**2、去 gitee 或者 github 上克隆一个测试！**

## 文件的四种状态（暂时了解）
版本控制就是对文件的版本控制，要对文件进行修改、提交等操作，首先要知道文件当前在什么状态，不然可能会提交了现在还不想提交的文件，或者要提交的文件没提交上。

- Untracked: 未跟踪, 此文件在文件夹中, 但并没有加入到git库, 不参与版本控制. 通过git add 状态变为Staged.
- Unmodify: 文件已经入库, 未修改, 即版本库中的文件快照内容与文件夹中完全一致. 这种类型的文件有两种去处, 如果它被修改, 而变为Modified. 如果使用git rm移出版本库, 则成为Untracked文件
- Modified: 文件已修改, 仅仅是修改, 并没有进行其他的操作. 这个文件也有两个去处, 通过git add可进入暂存staged状态, 使用git checkout 则丢弃修改过, 返回到unmodify状态, 这个git checkout即从库中取出文件, 覆盖当前修改 !
- Staged: 暂存状态. 执行git commit则将修改同步到库中, 这时库中的文件和本地文件又变为一致, 文件为Unmodify状态. 执行git reset HEAD filename取消暂存, 文件状态为Modified
## 查看文件状态
上面说文件有4种状态，通过如下命令可以查看到文件的状态：
**查看指定文件状态  $git status [filename]
查看所有文件状态  $git status
$git add .     （add后面有空格）添加所有文件到暂存区**
**$git commit -m "消息内容"    提交暂存区中的内容到本地仓库 -m 提交信息**

## 忽略文件(不需要上传的文件)
有些时候我们不想把某些文件纳入版本控制中，比如数据库文件，临时文件，设计文件等
在主目录下建立".gitignore"文件，此文件有如下规则：

1. 忽略文件中的空行或以井号（#）开始的行将会被忽略。
1. 可以使用Linux通配符。例如：星号（*）代表任意多个字符，问号（？）代表一个字符，方括号（[abc]）代表可选字符范围，大括号（{string1,string2,...}）代表可选的字符串等。
1. 如果名称的最前面有一个感叹号（!），表示例外规则，将不被忽略。
1. 如果名称的最前面是一个路径分隔符（/），表示要忽略的文件在此目录下，而子目录中的文件不忽略。
1. 如果名称的最后面是一个路径分隔符（/），表示要忽略的是此目录下该名称的子目录，而非文件（默认文件或目录都忽略）。

#为注释
*.txt        #忽略所有 .txt结尾的文件,这样的话上传就不会被选中！!lib.txt     #但lib.txt除外/temp        #仅忽略项目根目录下的TODO文件,不包括其它目录tempbuild/       #忽略build/目录下的所有文件
doc/*.txt    #会忽略 doc/notes.txt 但不包括 doc/server/arch.txt

# 使用码云
使用码云
github 是有墙的，比较慢，在国内的话，我们一般使用 gitee ，公司中有时候会搭建自己的gitlab服务器
这个其实可以作为大家未来找工作的一个重要信息！
1、注册登录码云，完善个人信息
![](https://cdn.nlark.com/yuque/0/2022/jpeg/27233047/1653719743902-3a12dbba-bfa0-481c-85f9-d0797a31c3be.jpeg#clientId=uec4524eb-b3b5-4&crop=0&crop=0&crop=1&crop=1&from=paste&id=u96d218aa&margin=%5Bobject%20Object%5D&originHeight=539&originWidth=1080&originalType=url&ratio=1&rotation=0&showTitle=false&status=done&style=none&taskId=u402db55e-c5cb-4483-ba11-55284cee0b4&title=)
2、设置本机绑定SSH公钥，实现免密码登录！（免密码登录，这一步挺重要的，码云是远程仓库，我们是平时工作在本地仓库！)

**# 进入 C:\Users\Administrator\.ssh 目录# 生成公钥ssh-keygen**
**# 生成公钥**
**ssh-keygen -t rsa**
![image.png](https://cdn.nlark.com/yuque/0/2022/png/27233047/1653719510918-697da731-ee79-4e38-ae44-1d49dd52ad4e.png#clientId=uec4524eb-b3b5-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=308&id=u5e11dd12&margin=%5Bobject%20Object%5D&name=image.png&originHeight=308&originWidth=895&originalType=binary&ratio=1&rotation=0&showTitle=false&size=31692&status=done&style=none&taskId=uc233af6b-7938-48c9-915c-5cbc26f542d&title=&width=895)
然后输入三次密码，记住密码。点击回车
自动创建两个文件，在.ssh目录中，打开某某.pub，用文本编辑器打开。
文本内容就是ssh公钥。
在网站getee个人设置中添加公钥。
确认时输入getee密码.

再在git编辑器中输入：
**ssh -T git@gitee.com**
**看看结果，是否已经将主机添加到本机ssh可信列表中了。**

## 使用码云创建一个新的仓库

![image.png](https://cdn.nlark.com/yuque/0/2022/png/27233047/1653721093313-57da900d-4a74-4d0a-8d37-5f43938e7586.png#clientId=uec4524eb-b3b5-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=715&id=u264b0dbe&margin=%5Bobject%20Object%5D&name=image.png&originHeight=715&originWidth=906&originalType=binary&ratio=1&rotation=0&showTitle=false&size=56607&status=done&style=none&taskId=u0380ff36-64da-4e58-ac75-87b1a12edfb&title=&width=906)
许可证：开源是否可以随意转载，开源但是不能商业使用，不能转载，…限制！
![image.png](https://cdn.nlark.com/yuque/0/2022/png/27233047/1653721334553-9b5db6ee-6d28-4eb8-a456-8177d44de4bd.png#clientId=uec4524eb-b3b5-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=443&id=uefad6760&margin=%5Bobject%20Object%5D&name=image.png&originHeight=443&originWidth=1119&originalType=binary&ratio=1&rotation=0&showTitle=false&size=66821&status=done&style=none&taskId=uaa00bb2b-140f-4b68-88e6-a6e85967156&title=&width=1119)
![image.png](https://cdn.nlark.com/yuque/0/2022/png/27233047/1653721307759-07a25ed4-d6ee-4422-a507-911f86aca094.png#clientId=uec4524eb-b3b5-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=405&id=ue202dad1&margin=%5Bobject%20Object%5D&name=image.png&originHeight=405&originWidth=751&originalType=binary&ratio=1&rotation=0&showTitle=false&size=37110&status=done&style=none&taskId=udaa659ed-3e2b-471b-8315-08d92cf71cb&title=&width=751)
克隆到本地---成功！！！


## IDEA中集成Git
新建一个Springboot项目。
![image.png](https://cdn.nlark.com/yuque/0/2022/png/27233047/1653722559584-5d3cfe0a-eb6a-424d-b20d-fa5e2246903d.png#clientId=uec4524eb-b3b5-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=836&id=u248c2c63&margin=%5Bobject%20Object%5D&name=image.png&originHeight=836&originWidth=822&originalType=binary&ratio=1&rotation=0&showTitle=false&size=69690&status=done&style=none&taskId=uf88bc6c7-168c-406d-b022-0a5278708f5&title=&width=822)
![image.png](https://cdn.nlark.com/yuque/0/2022/png/27233047/1653722582778-9c6a51a7-1d04-4a65-bdbd-4418d03f4f2b.png#clientId=uec4524eb-b3b5-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=724&id=u956bc060&margin=%5Bobject%20Object%5D&name=image.png&originHeight=724&originWidth=849&originalType=binary&ratio=1&rotation=0&showTitle=false&size=62366&status=done&style=none&taskId=u116dfddd-ce64-45d0-a61d-d3dfbe6bf27&title=&width=849)
等待加载完成。
![image.png](https://cdn.nlark.com/yuque/0/2022/png/27233047/1653722695423-32412273-dcb7-4e7c-b63a-d97790ee7107.png#clientId=uec4524eb-b3b5-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=598&id=ud440c606&margin=%5Bobject%20Object%5D&name=image.png&originHeight=598&originWidth=905&originalType=binary&ratio=1&rotation=0&showTitle=false&size=40969&status=done&style=none&taskId=u52ef9bf4-093a-424c-a631-017b3c14763&title=&width=905)
方法一：将之前git创建的工程文件copy到工程中。---最简单
方法二：将工程目录考到git工程文件夹下。![image.png](https://cdn.nlark.com/yuque/0/2022/png/27233047/1653723028475-9ae5ff24-a06c-4369-a46f-3f0ccd4e107a.png#clientId=uec4524eb-b3b5-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=447&id=u6fb57c1e&margin=%5Bobject%20Object%5D&name=image.png&originHeight=447&originWidth=1330&originalType=binary&ratio=1&rotation=0&showTitle=false&size=314417&status=done&style=none&taskId=u829117b1-9da3-4b5d-8754-c2260deea74&title=&width=1330)
我们要修改文件，并且提交。

1.手动可视化提交
![image.png](https://cdn.nlark.com/yuque/0/2022/png/27233047/1653723613539-3bb0095a-c994-4708-ae17-e434cea4d1ee.png#clientId=uec4524eb-b3b5-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=855&id=u88575e7b&margin=%5Bobject%20Object%5D&name=image.png&originHeight=855&originWidth=1293&originalType=binary&ratio=1&rotation=0&showTitle=false&size=415975&status=done&style=none&taskId=u0a9475e8-7ef9-4bbc-ad03-0acbbdae5da&title=&width=1293)
![image.png](https://cdn.nlark.com/yuque/0/2022/png/27233047/1653723820997-05a3bfd2-f49a-4673-9d4d-7c98904ae8a0.png#clientId=uec4524eb-b3b5-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=406&id=u6e5d59f0&margin=%5Bobject%20Object%5D&name=image.png&originHeight=406&originWidth=1021&originalType=binary&ratio=1&rotation=0&showTitle=false&size=228561&status=done&style=none&taskId=u4dfd7820-a8fc-4a24-b39d-dcd072e3f56&title=&width=1021)
![image.png](https://cdn.nlark.com/yuque/0/2022/png/27233047/1653723860231-aaf8190e-48c3-4867-bd49-2ebfe25e2a16.png#clientId=uec4524eb-b3b5-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=527&id=ufec74d2b&margin=%5Bobject%20Object%5D&name=image.png&originHeight=527&originWidth=766&originalType=binary&ratio=1&rotation=0&showTitle=false&size=213561&status=done&style=none&taskId=u7a75774d-bb2f-4b86-89aa-2d8cb55d6bd&title=&width=766)
![image.png](https://cdn.nlark.com/yuque/0/2022/png/27233047/1653724118252-f26faa01-a114-43cf-b350-b8053a92261f.png#clientId=uec4524eb-b3b5-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=733&id=u6c8db6b5&margin=%5Bobject%20Object%5D&name=image.png&originHeight=733&originWidth=1172&originalType=binary&ratio=1&rotation=0&showTitle=false&size=500043&status=done&style=none&taskId=u91276173-2e9e-4a9b-897c-4c69a3db812&title=&width=1172)
2.用代码方式提交
控制台：
git add .
git commit -m "文本备注信息"
git push

## 团队很重要
1.不要把git想的很难。
2.git学习方式很多

CIT分支很重要
![image.png](https://cdn.nlark.com/yuque/0/2022/png/27233047/1653724889296-c37fa0d1-55e6-4921-a1e8-8525917ff9cf.png#clientId=uec4524eb-b3b5-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=587&id=u152ef5f9&margin=%5Bobject%20Object%5D&name=image.png&originHeight=587&originWidth=629&originalType=binary&ratio=1&rotation=0&showTitle=false&size=248407&status=done&style=none&taskId=u3d7acc64-5a28-4985-8a68-1e34ea533ec&title=&width=629)
![](https://cdn.nlark.com/yuque/0/2022/png/27233047/1653725500083-874fbd10-ea85-406c-a57c-bec9d7c13e03.png#clientId=uec4524eb-b3b5-4&crop=0&crop=0&crop=1&crop=1&from=paste&id=u7e66436a&margin=%5Bobject%20Object%5D&originHeight=346&originWidth=729&originalType=url&ratio=1&rotation=0&showTitle=false&status=done&style=none&taskId=u46a5494a-d08c-489b-a761-05812bc2daa&title=)
git分支中常用指令：
列出所有本地分支：git branch
列出所有远程分支git branch -r
新建一个分支，但依然停留在当前分支git branch [branch-name]
新建一个分支，并切换到该分支git checkout -b [branch]
合并指定分支到当前分支$ git merge [branch]
删除分支$ git branch -d [branch-name]
删除远程分支
$ git push origin --delete [branch-name]
$ git branch -dr [remote/branch]
IDEA中操作

![](https://cdn.nlark.com/yuque/0/2022/png/27233047/1653725499990-71aa24ef-a676-43fa-baa8-58d473b99795.png#clientId=uec4524eb-b3b5-4&crop=0&crop=0&crop=1&crop=1&from=paste&id=u6b714217&margin=%5Bobject%20Object%5D&originHeight=455&originWidth=619&originalType=url&ratio=1&rotation=0&showTitle=false&status=done&style=none&taskId=u01ba71ce-01c4-4ca3-b138-90d8b58804e&title=)
如果同一个文件在合并分支时都被修改了则会引起冲突：
解决的办法是我们可以修改冲突文件后重新提交！选择要保留他的代码还是你的代码！

master主分支应该非常稳定，用来发布新版本，一般情况下不允许在上面工作，工作一般情况下在新建的dev分支上工作，工作完后，比如上要发布，或者说dev分支代码稳定后可以合并到主分支master上来。

作业练习：找一个小伙伴，一起搭建一个远程仓库，来练习Git！

